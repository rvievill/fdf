/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_map.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/16 15:37:19 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/17 11:58:28 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

void			update_map(int **tab, t_point *p, int nb)
{
	int			i;
	int			j;

	i = 0;
	while (i < p->map_y)
	{
		j = 0;
		while (j < p->map_x)
		{
			tab[i][j] = tab[i][j] + nb;
			j++;
		}
		i++;
	}
}

void			update_size(t_point *p, int keycode)
{
	if (keycode == ZOOM)
		p->space = p->space + (p->space / 3);
	else if (keycode == DZOOM)
		p->space = p->space - (p->space / 3);
	fill_pos_x(p);
	fill_pos_y(p);
}
