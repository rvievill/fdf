/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_tab_pos.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 12:08:47 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/17 11:59:05 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

static void			malloc_tab(int ***tab, int line, int col)
{
	int				i;

	i = 0;
	*tab = (int **)malloc(sizeof(int *) * line);
	while (i < line)
	{
		(*tab)[i] = (int *)malloc(sizeof(int) * col);
		i++;
	}
}

void				fill_pos_x(t_point *inf)
{
	int				i;
	int				j;
	int				mid_win;
	int				mid_map;
	int				space;

	i = 0;
	space = (inf->space / 2 > 1) ? (inf->space / 2) : inf->space;
	mid_win = inf->win_x / 2;
	mid_map = inf->map_x / 2;
	inf->pos_x[0][0] = mid_win - (mid_map * space);
	while (i < inf->map_y)
	{
		j = 1;
		while (j < inf->map_x)
		{
			inf->pos_x[i][j] = inf->pos_x[i][j - 1] + space;
			j++;
		}
		i++;
		if (i != inf->map_y)
			inf->pos_x[i][0] = inf->pos_x[i - 1][0] + space;
	}
}

void				fill_pos_y(t_point *inf)
{
	int				i;
	int				j;
	int				mid_win;
	int				mid_map;
	int				space;

	i = 0;
	space = (inf->space / 4 > 0) ? (inf->space / 4) : inf->space;
	mid_win = inf->win_y / 2;
	mid_map = inf->map_y / 2;
	inf->pos_y[0][0] = mid_win - (mid_map * space);
	while (i < inf->map_y)
	{
		j = 1;
		while (j < inf->map_x)
		{
			inf->pos_y[i][j] = inf->pos_y[i][j - 1] - space;
			j++;
		}
		i++;
		if (i != inf->map_y)
			inf->pos_y[i][0] = inf->pos_y[i - 1][0] + space;
	}
}

void				fill_tab_pos(t_point *point)
{
	int				i;

	i = 0;
	malloc_tab(&point->pos_x, point->map_y, point->map_x);
	malloc_tab(&point->pos_y, point->map_y, point->map_x);
	fill_pos_y(point);
	fill_pos_x(point);
}
