/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tracing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 16:58:48 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/16 14:27:17 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

void			tracing(t_point *inf, int tab[4])
{
	int			dx;
	int			sx;
	int			dy;
	int			sy;
	int			err;

	dx = abs(tab[2] - tab[0]);
	dy = abs(tab[3] - tab[1]);
	sx = (tab[0] < tab[2] ? 1 : -1);
	sy = (tab[1] < tab[3] ? 1 : -1);
	err = (dx > dy ? dx : -dy) / 2;
	while (tab[0] != tab[2] && tab[1] != tab[3])
	{
		mlx_pixel_put(inf->mlx, inf->win, tab[0], tab[1], 0x00FFFFFF);
		if (err > -dx)
		{
			err -= dy;
			tab[0] += sx;
		}
		if (err < dy)
		{
			err += dx;
			tab[1] += sy;
		}
	}
}

void			tracing_debug(t_point *inf, int tab[4])
{
	int			dx;
	int			sx;
	int			dy;
	int			sy;
	int			err;

	dx = abs(tab[2] - tab[0]);
	dy = abs(tab[3] - tab[1]);
	sx = (tab[0] < tab[2] ? 1 : -1);
	sy = (tab[1] < tab[3] ? 1 : -1);
	err = (dx > dy ? dx : -dy) / 2;
	while (tab[0] != tab[2] && tab[1] != tab[3])
	{
		mlx_pixel_put(inf->mlx, inf->win, tab[0], tab[1], 0x00FF0000);
		if (err > -dx)
		{
			err -= dy;
			tab[0] += sx;
		}
		if (err < dy)
		{
			err += dx;
			tab[1] += sy;
		}
	}
}
