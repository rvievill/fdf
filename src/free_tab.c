/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_tab.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/16 15:27:17 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/16 16:20:12 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

void			free_tab(int **tab)
{
	int			i;

	i = 0;
	if (*tab[i])
	{
		free(tab[i]);
		i++;
	}
	free(tab);
}

void			free_struct(t_point *point)
{
	free_tab(point->tab);
	free_tab(point->pos_x);
	free_tab(point->pos_y);
	free(point);
}
