/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 16:33:48 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/16 15:43:32 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

int			nb_per_line(char *line)
{
	int		i;
	int		nb;

	i = 0;
	nb = 0;
	while (line[i])
	{
		if (line[i] != ' ' && line[i] != '\t' &&
			(line[i + 1] == ' ' || line[i + 1] == '\t' || line[i + 1] == '\0'))
			nb++;
		i++;
	}
	return (nb);
}

int			parser(char **av, int *size, int *nb_line)
{
	int		fd;
	char	*line;

	line = NULL;
	fd = open(av[1], O_RDONLY);
	get_next_line(fd, &line);
	*size = nb_per_line(line);
	(*nb_line)++;
	line = NULL;
	while (get_next_line(fd, &line) == 1)
	{
		(*nb_line)++;
		if (*size != nb_per_line(line))
			return (1);
		line = NULL;
	}
	return (0);
}
