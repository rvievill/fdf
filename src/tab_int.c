/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab_int.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 14:05:57 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/16 15:44:18 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

void			fill_tab_int(int ***new_tab, char **tab, int line, int col)
{
	int			i;
	int			j;
	int			k;

	i = 0;
	while (i < line)
	{
		j = 0;
		k = 0;
		while (j < col)
		{
			while (tab[i][k] && (tab[i][k] == ' ' || tab[i][k] == '\t'))
				k++;
			(*new_tab)[i][j] = ft_atoi(&tab[i][k]);
			while (tab[i][k] && tab[i][k] != ' ' && tab[i][k] != '\t')
				k++;
			j++;
		}
		i++;
	}
}

int				**create_tab_int(char **tab, int col, int line)
{
	int			**new_tab;
	int			i;

	new_tab = NULL;
	i = -1;
	new_tab = (int **)malloc(sizeof(int *) * line);
	while (++i < line)
		new_tab[i] = (int *)malloc(sizeof(int) * col);
	fill_tab_int(&new_tab, tab, line, col);
	return (new_tab);
}
