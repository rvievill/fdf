/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab_char.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 14:22:43 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/15 18:11:02 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

char			**create_tab_char(char *file, int nb_line)
{
	char		**tab;
	int			fd;
	char		*line;
	int			i;

	i = -1;
	line = NULL;
	if (!(tab = (char **)malloc(sizeof(char *) * (nb_line + 1))))
		return (NULL);
	tab[nb_line] = 0;
	if ((fd = open(file, O_RDONLY)) == -1)
		return (NULL);
	get_next_line(fd, &line);
	tab[++i] = ft_strdup(line);
	line = NULL;
	while (get_next_line(fd, &line) == 1)
	{
		tab[++i] = ft_strdup(line);
		line = NULL;
	}
	return (tab);
}
