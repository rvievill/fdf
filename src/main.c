/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 16:25:52 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/16 13:40:13 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

static int			error(int nb)
{
	if (nb == 1)
		ft_putendl("fdf: parse error");
	return (nb);
}

static void			fill_struct(t_point *point, int col, int line)
{
	point->map_x = col;
	point->map_y = line;
	point->win_x = 2000;
	point->win_y = 1000;
	point->space = point->win_x / point->map_x / 2;
	if (point->space <= 0)
		point->space = 3;
}

int					main(int ac, char **av)
{
	int				line;
	int				col;
	char			**tab;
	t_point			*point;

	if (ac == 2)
	{
		line = 0;
		col = 0;
		point = (t_point *)malloc(sizeof(t_point));
		if (parser(av, &col, &line) == 1)
			return (error(1));
		fill_struct(point, col, line);
		tab = create_tab_char(av[1], point->map_y);
		point->tab = create_tab_int(tab, point->map_x, point->map_y);
		fill_tab_pos(point);
		display_map(point, av[1]);
	}
	else
		ft_putendl("NTM (groupe de rap ou pas ...) !");
	return (0);
}
