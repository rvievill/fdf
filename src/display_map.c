/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_map.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 15:24:18 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/17 11:49:09 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/fdf.h"

static void				tracing_y(t_point *p, int i, int j)
{
	int					tab[4];

	tab[0] = p->pos_x[i][j];
	tab[1] = p->pos_y[i][j] - (p->tab[i][j] * (p->space / 2));
	tab[2] = p->pos_x[i + 1][j];
	tab[3] = p->pos_y[i + 1][j] - (p->tab[i + 1][j] * (p->space / 2));
	tracing(p, tab);
}

static void				tracing_x(t_point *p, int i, int j)
{
	int					tab[4];

	tab[0] = p->pos_x[i][j];
	tab[1] = p->pos_y[i][j] - (p->tab[i][j] * (p->space / 2));
	tab[2] = p->pos_x[i][j + 1];
	tab[3] = p->pos_y[i][j + 1] - (p->tab[i][j + 1] * (p->space / 2));
	tracing(p, tab);
}

static void				display(t_point *point)
{
	int					i;
	int					j;

	i = 0;
	while (i < point->map_y)
	{
		j = 0;
		while (j < point->map_x)
		{
			if (j < point->map_x - 1)
				tracing_x(point, i, j);
			if (i < point->map_y - 1)
				tracing_y(point, i, j);
			j++;
		}
		i++;
	}
}

static int				key_hook(int keycode, t_point *p)
{
	if (keycode == ESC)
	{
		free_struct(p);
		mlx_destroy_window(p->mlx, p->win);
		exit(0);
	}
	else if (keycode == LEFT)
		update_map(p->pos_x, p, -20);
	else if (keycode == RIGHT)
		update_map(p->pos_x, p, 20);
	else if (keycode == DOWN)
		update_map(p->pos_y, p, 20);
	else if (keycode == UP)
		update_map(p->pos_y, p, -20);
	if (keycode == ZOOM)
		update_size(p, keycode);
	if (keycode == DZOOM)
		update_size(p, keycode);
	mlx_clear_window(p->mlx, p->win);
	display(p);
	return (0);
}

void					display_map(t_point *point, char *name)
{
	point->mlx = mlx_init();
	point->win = mlx_new_window(point->mlx, 2000, 1000, name);
	display(point);
	mlx_key_hook(point->win, key_hook, point);
	mlx_loop(point->mlx);
}
