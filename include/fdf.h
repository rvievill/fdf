/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 16:26:17 by rvievill          #+#    #+#             */
/*   Updated: 2016/09/17 11:46:19 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <mlx.h>
# include <math.h>
# include <sys/types.h>
# include <unistd.h>
# include <fcntl.h>
# include "../libft/libft.h"

# include <stdio.h>

typedef struct		s_point
{
	void			*mlx;
	void			*win;
	int				**tab;
	int				**pos_x;
	int				**pos_y;
	int				space;
	int				win_x;
	int				win_y;
	int				map_x;
	int				map_y;
}					t_point;

enum				e_key_code
{
	ESC = 53,
	LEFT = 123,
	RIGHT = 124,
	UP = 126,
	DOWN = 125,
	ZOOM = 69,
	DZOOM = 78
}					;
/*
** update_map.c
*/
void				update_map(int **tab, t_point *p, int nb);
void				update_size(t_point *p, int keycode);
/*
** free_tab.c
*/
void				free_tab(int **tab);
void				free_struct(t_point *point);
/*
** tracing.c
*/
void				tracing(t_point *inf, int tab[4]);
void				tracing_debug(t_point *inf, int tab[4]);
/*
** fill_tab_pos.c
*/
void				fill_tab_pos(t_point *point);
void				fill_pos_x(t_point *inf);
void				fill_pos_y(t_point *inf);
/*
** parser.c
*/
int					parser(char **av, int *size, int *nb_line);
/*
** tab_int.c
*/
int					**create_tab_int(char **av, int col, int line);
/*
** tab_char.c
*/
char				**create_tab_char(char *file, int line);
/*
** display_map.c
*/
void				display_map(t_point *point, char *name);

#endif
